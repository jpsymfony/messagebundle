<?php

namespace JpSymfony\MessageBundle\ApiClient;

use JpSymfony\UserBundle\ApiClient\ApiClient;

class ApiMessageClient extends ApiClient
{
    public function __construct(
        string $apiMessageBaseUri,
        string $clientSource
    ) {
        parent::__construct($apiMessageBaseUri, $clientSource);
    }
}
