<?php

namespace JpSymfony\MessageBundle\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints;

class NewAccountConfirmedEmail implements EmailInterface
{
    /**
     * @Groups({"api_mail"})
     *
     * @Constraints\NotBlank()
     */
    private string $uuid;

    /**
     * @Groups({"api_mail"})
     *
     * @Constraints\NotBlank()
     */
    private string $nickname;

    /**
     * @Groups({"api_mail"})
     *
     * @Constraints\NotBlank()
     */
    private string $email;

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
