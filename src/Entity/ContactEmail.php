<?php

namespace JpSymfony\MessageBundle\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints;

class ContactEmail implements EmailInterface
{
    /**
     * @Groups({"api_mail"})
     */
    private string $uuid;

    /**
     * @Groups({"api_mail"})
     *
     * @Constraints\NotBlank(message="contact.fullName.notblank")
     */
    private string $fullName;

    /**
     * @Groups({"api_mail"})
     *
     * @Constraints\NotBlank(message="contact.email.notblank")
     * @Constraints\Email(message="contact.email.invalid")
     */
    private string $email;

    /**
     * @Groups({"api_mail"})
     *
     * @Constraints\NotBlank(message="contact.body.notblank")
     * @Constraints\Length(min=20, minMessage="contact.body.too_short")
     */
    private string $body;

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }
}
