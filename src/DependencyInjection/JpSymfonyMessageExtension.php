<?php

namespace JpSymfony\MessageBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class JpSymfonyMessageExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('api_message_base_uri', $config['api_message_base_uri']);
        $container->setParameter('client_source', $config['client_source']);
        $container->setParameter('x_auth_token', $config['x_auth_token']);

        $loader->load('services.yaml');
    }
}
