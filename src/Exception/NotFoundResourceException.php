<?php

namespace JpSymfony\MessageBundle\Exception;

use Symfony\Component\HttpFoundation\Response;

class NotFoundResourceException extends \RuntimeException
{
    public function __construct(string $message, int $code = Response::HTTP_INTERNAL_SERVER_ERROR, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
