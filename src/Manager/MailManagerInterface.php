<?php

namespace JpSymfony\MessageBundle\Manager;

use JpSymfony\MessageBundle\Entity\EmailInterface;
use JpSymfony\MessageBundle\ValueObject\ResponseVO;

interface MailManagerInterface
{
    public function sendEmail(EmailInterface $email, string $url): ResponseVO;

    public function getEmail(array $query, string $url, string $emailClass): ResponseVO;

    public function deleteEmail(string $url, string $id): ResponseVO;
}
