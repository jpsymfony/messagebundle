<?php

namespace JpSymfony\MessageBundle\Manager;

use JpSymfony\MessageBundle\Entity\EmailInterface;
use JpSymfony\MessageBundle\Helper\HeaderHelper;
use JpSymfony\MessageBundle\Repository\MailRepository;
use JpSymfony\MessageBundle\ValueObject\ResponseVO;

class MailManager implements MailManagerInterface
{
    //todo type with interface
    private MailRepository $mailRepository;
    private HeaderHelper $headerHelper;

    public function __construct(
        MailRepository $mailRepository,
        HeaderHelper $headerHelper
    ) {
        $this->mailRepository = $mailRepository;
        $this->headerHelper = $headerHelper;
    }

    public function sendEmail(EmailInterface $email, string $url): ResponseVO
    {
        return $this->mailRepository->sendEmail(
            $email,
            $url,
            $this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN),
        );
    }

    public function getEmail(array $query, string $url, string $emailClass): ResponseVO
    {
        return $this->mailRepository->getEmail(
            $query,
            $url,
            $emailClass,
            $this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN),
        );
    }

    public function deleteEmail(string $url, string $id): ResponseVO
    {
        return $this->mailRepository->deleteEmail(
            $url,
            $this->headerHelper->getHeadersFromHeaderType(HeaderHelper::X_AUTH_TOKEN),
            $id
        );
    }
}
