<?php

namespace JpSymfony\MessageBundle\ValueObject;

use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Response;

class ResponseVO
{
    private int $code;
    private ?FormErrorIterator $formErrorIterator;
    private ?array $apiFormErrors;
    private ?string $errorMessage;
    private ?object $data;

    public function __construct(
        int $code,
        ?FormErrorIterator $formErrorIterator,
        ?array $apiFormErrors,
        ?string $errorMessage,
        ?object $data
    ) {
        $this->code = $code;
        $this->formErrorIterator = $formErrorIterator;
        $this->apiFormErrors = $apiFormErrors;
        $this->errorMessage = $errorMessage;
        $this->data = $data;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function isCodeOk(): bool
    {
        return Response::HTTP_OK === $this->code;
    }

    public function isCodeCreated(): bool
    {
        return Response::HTTP_CREATED === $this->code;
    }

    public function isUnprocessableEntity(): bool
    {
        return Response::HTTP_UNPROCESSABLE_ENTITY === $this->code;
    }

    public function getApiFormErrors(): ?array
    {
        return $this->apiFormErrors;
    }

    public function getFormErrorIterator(): ?FormErrorIterator
    {
        return $this->formErrorIterator;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function getData(): ?object
    {
        return $this->data;
    }
}
