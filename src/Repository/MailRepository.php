<?php

namespace JpSymfony\MessageBundle\Repository;

use JpSymfony\MessageBundle\ApiClient\ApiMessageClient;
use JpSymfony\MessageBundle\Entity\EmailInterface;
use JpSymfony\MessageBundle\ValueObject\ResponseVO;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class MailRepository
{
    public const BASE_URL = '/api';

    private ApiMessageClient $apiMessageClient;
    private SerializerInterface $serializer;
    private LoggerInterface $logger;

    public function __construct(
        ApiMessageClient $apiMessageClient,
        SerializerInterface $serializer,
        LoggerInterface $logger
    ) {
        $this->apiMessageClient = $apiMessageClient;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function sendEmail(EmailInterface $email, string $url, array $headers): ResponseVO
    {
        try {
            $responseVO = $this->apiMessageClient->getClient()->request(
                'POST',
                static::BASE_URL . $url,
                [
                    'json' => json_decode(
                        $this->serializer->serialize($email, 'json', ['groups' => 'api_mail']),
                        true
                    ),
                    'headers' => $headers,
                ]
            );

            return new ResponseVO(
                Response::HTTP_CREATED,
                null,
                null,
                null,
                $this->serializer->deserialize(
                    $responseVO->getContent(),
                    \get_class($email),
                    'json',
                    ['groups' => 'api_mail']
                )
            );
        } catch (ClientException $e) {
            $response = json_decode($e->getResponse()->getContent(false));

            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $response->detail ?? $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                $response->violations ?? null,
                $response->detail ?? $e->getMessage(),
                $response
            );
        } catch (ServerException $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        }
    }

    public function getEmail(array $query, string $url, string $emailClass, array $headers, int $limit = 1): ResponseVO
    {
        try {
            $response = $this->apiMessageClient->getClient()->request(
                'GET',
                static::BASE_URL . $url,
                [
                    'query' => $query,
                    'headers' => $headers,
                ]
            );

            $response = json_decode($response->getContent(), true);

            if (!empty($response)) {
                if (1 === $limit) {
                    $response = end($response);
                }

                return new ResponseVO(
                    Response::HTTP_OK,
                    null,
                    null,
                    null,
                    $this->serializer->deserialize(
                        json_encode($response),
                        $emailClass,
                        'json',
                        ['groups' => 'api_mail']
                    )
                );
            }
        } catch(ClientException $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $response->detail ?? $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        } catch (ServerException $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        }

        return new ResponseVO(
            Response::HTTP_NOT_FOUND,
            null,
            null,
            sprintf('Message not found: %s', implode(',', $query)),
            null
        );
    }

    public function deleteEmail(string $url, array $headers, string $messageId): ResponseVO
    {
        try {
            $this->apiMessageClient->getClient()->request(
                'DELETE',
                sprintf('%s/%s', static::BASE_URL . $url, $messageId),
                [
                    'headers' => $headers,
                ]
            );

            return new ResponseVO(
                Response::HTTP_NO_CONTENT,
                null,
                null,
                null,
                null
            );
        } catch(ClientException $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $response->detail ?? $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        } catch (ServerException $e) {
            $this->logger->error(
                sprintf('%s: An error occurred', static::class),
                [
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                ]
            );

            return new ResponseVO(
                $e->getCode(),
                null,
                null,
                $e->getMessage(),
                null
            );
        }
    }
}
