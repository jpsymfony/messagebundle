build_docker: build up composer

build:
	docker-compose build php

up:
	docker-compose up -d

stop:
	docker-compose stop && docker container prune --force

ssh:
	docker-compose exec php bash

composer:
	docker-compose exec php bash -c 'composer install --no-interaction -o'

clean_docker:
	docker image prune --force && docker container prune --force

test_phpunit:
	docker-compose exec php bash -c 'vendor/bin/phpunit'

.PHONY: build up ssh composer stop clean_docker test_phpunit
